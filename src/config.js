export default {
  pressureThreshold: [
    {
      text: 'Dark Blue',
      style: {
        background: '#192446',
        color: '#fff'
      },
      max: 15
    },
    {
      text: 'Blue',
      style: {
        background: '#386da7',
        color: '#fff'
      },
      min: 16,
      max: 30
    },
    {
      text: 'Green',
      style: {
        background: '#095611',
        color: '#fff'
      },
      min: 31,
      max: 45
    },
    {
      text: 'Yellow',
      style: {
        background: '#f6e339',
        color: '#000000'
      },
      min: 31,
      max: 60
    },
    {
      text: 'Orange',
      style: {
        background: '#c17e12',
        color: '#fff'
      },
      min: 61,
      max: 75
    },
    {
      text: 'Red',
      style: {
        background: '#bb0010',
        color: '#fff'
      },
      min: 76
    }
  ]
}
