import Vue from 'vue'
import Router from 'vue-router'
import PrepareReport from './views/PrepareReport.vue'
import Report from './views/Report.vue'
import store from './store'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'prepare-report',
      component: PrepareReport
    },
    {
      path: '/report',
      name: 'report',
      component: Report,
      beforeEnter: (to, from, next) => {
        const isFileUploaded = store.state.generalInformation.date.length
        if (isFileUploaded) {
          next()
        } else {
          next('/')
        }
      }
    }
  ]
})
