import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    rawData: [],
    generalInformation: {
      name: '',
      mrn: '',
      date: ''
    },
    insights: {
      insight1: 0,
      insight2: 0,
      insight3: 0,
      insight4: 0
    },
    events: {
      outOffBed: [],
      systemOff: [],
      repositionInterval: [],
      timeInPosition: [],
      pressureThreshold: [],
      turn: []
    },
    settings: {
      hoursInterval: true,
      repositionInsightIncludeAll: true,
      repositionHourly: true,
      timeInPositionHourly: true
    }
  },
  mutations: {
    updateSettings (state, newSettings) {
      state.settings = newSettings
    },
    fromCsv (state, data) {
      function parseTime (timeStr) {
        const hours = parseInt(timeStr.split(':')[0], 10)
        const minutes = parseInt(timeStr.split(':')[1], 10)

        return hours * 60 + minutes
      }
      /* 0:"Details"
       1:"Values"
       2:"DATETIME"
       3:"STATUS"
       4:"MAXPRESSURE"
       5:"PRESSURE_THRESHOLD"
       6:"INTERVAL"
       7:"Avg Peak Pressure"
       8:"System Status"
       9:"Time In Position"
       10:"Turn Type"
       11:"Timer Reset"
       12:"New Position Below Thresold" */
      state.rawData = data

      state.generalInformation.name = data[4][1]
      state.generalInformation.mrn = data[2][1]
      state.generalInformation.date = data[1][1]

      state.events.outOffBed = []
      state.events.systemOff = []
      state.events.repositionInterval = []
      state.events.timeInPosition = []
      state.events.pressureThreshold = []
      state.events.turn = []

      let dataFiltered = data
        .filter(row => { return row[2] || row[3] || row[4] || row[5] || row[6] || row[7] || row[8] })

      let endDay = 24*60 // end of day
      let maxTime = 0
      for (let i = 0; i < dataFiltered.length; i++) {
        const time = parseTime(dataFiltered[i][2])
        if (!isNaN(time)) maxTime = time
      }
      maxTime += 30

      if (maxTime >= 23*60 + 30) {
        maxTime = endDay
      }

      // Fill system off end of day
      if (maxTime < endDay) {
        state.events.systemOff.push({ startTime: maxTime, endTime: endDay })
      }

      for (let i = 0; i < dataFiltered.length; i++) {
        const time = parseTime(dataFiltered[i][2])
        const eventStatus = dataFiltered[i][3].toLowerCase()
        const maxPressure = dataFiltered[i][4]
        const pressureThreshold = dataFiltered[i][5]
        const interval = dataFiltered[i][6]
        const systemStatus = dataFiltered[i][8].toLowerCase()
        const turnType = dataFiltered[i][10].toLowerCase()
        const timerReset = dataFiltered[i][11].toLowerCase()

        if (isNaN(time)) continue

        // === Out Of Bed ===
        if (systemStatus === 'out of bed' && dataFiltered[i - 1][8].toLowerCase() !== 'out of bed') {
          const startTime = time
          let endTime = maxTime
          for (let j = i + 1; j < dataFiltered.length; j++) {
            let jSystemStatus = dataFiltered[j][8].toLowerCase()
            let jTime = parseTime(dataFiltered[j][2])
            if (jSystemStatus !== 'out of bed') {
              endTime = jTime
              break
            }
          }

          state.events.outOffBed.push({ startTime, endTime })
        }

        // === System Off ===
        if (systemStatus === 'system off') {
          const startTime = time
          let endTime = endDay
          for (let j = i + 1; j < dataFiltered.length; j++) {
            let jSystemStatus = dataFiltered[j][8].toLowerCase()
            let jTime = parseTime(dataFiltered[j][2])
            if (jSystemStatus !== 'system off') {
              endTime = jTime
              break
            }
          }

          state.events.systemOff.push({ startTime, endTime })
        }

        // === Interval ===
        if (interval) {
          const intervalMinutes = interval.toLowerCase() === 'off' ? null : parseFloat(interval) * 60
          const startTime = time
          let endTime = maxTime
          console.log('endTime1', endTime)
          let followByAnotherArrow = false
          for (let j = i + 1; j < dataFiltered.length; j++) {
            let jInterval = dataFiltered[j][6]
            let jSystemStatus = dataFiltered[j][8].toLowerCase()
            let jTime = parseTime(dataFiltered[j][2])
            if (jSystemStatus === 'system off' || jSystemStatus === 'out of bed' || jInterval) {
              endTime = jTime
              followByAnotherArrow = !jSystemStatus
              break
            }
          }
          console.log('endTime2', endTime)

          state.events.repositionInterval.push({ startTime, endTime, intervalMinutes, followByAnotherArrow })
        }

        // === Pressure Threshold ===
        if (pressureThreshold) {
          const startTime = time
          let endTime = maxTime
          let followByAnotherArrow = false
          for (let j = i + 1; j < dataFiltered.length; j++) {
            let jPressureThreshold = dataFiltered[j][5]
            let jSystemStatus = dataFiltered[j][8].toLowerCase()
            let jTime = parseTime(dataFiltered[j][2])
            if (jSystemStatus === 'system off' || jSystemStatus === 'out of bed' || jPressureThreshold) {
              endTime = jTime
              followByAnotherArrow = !jSystemStatus
              break
            }
          }

          state.events.pressureThreshold.push({ startTime, endTime, pressureThreshold, followByAnotherArrow })
        }

        // === Turn ===
        if (eventStatus) {
          // Find bellowThreshold
          let bellowThreshold = 'none'
          for (let j = i - 1; j >= 0; j--) {
            const pressureThreshold = dataFiltered[j][5]
            if (pressureThreshold) {
              if (pressureThreshold > maxPressure) { bellowThreshold = 'yes' } else if (pressureThreshold < maxPressure) { bellowThreshold = 'no' }

              break
            }
          }

          // === Find overdueDuration
          let overdueDuration = null

          // Find lastIntervalMinutes
          let lastIntervalMinutes = null
          for (let j = i - 1; j >= 0; j--) {
            const jInterval = dataFiltered[j][6].toLowerCase()
            if (jInterval) {
              if (parseFloat(jInterval)) { lastIntervalMinutes = parseFloat(jInterval) * 60 }

              break
            }
          }

          // Find lastEventTime
          let lastEventTime = null
          for (let j = i - 1; j >= 0; j--) {
            const jEventStatus = dataFiltered[j][3].toLowerCase()
            const jTime = parseTime(dataFiltered[j][2].toLowerCase())
            const jSystemStatus = dataFiltered[j][8].toLowerCase()

            if (isNaN(jTime) || jSystemStatus === 'out of bed' || jSystemStatus === 'system off') { break }

            if (jEventStatus) {
              lastEventTime = jTime
              break
            }
          }

          if (lastEventTime && lastIntervalMinutes && time - lastEventTime > lastIntervalMinutes) { overdueDuration = time - lastEventTime }

          // Check "Time In Position" for first turn
          if (state.events.turn.length === 0 && lastIntervalMinutes) {
            const timeInPosition = data[6][10]
            if (time + timeInPosition > lastIntervalMinutes) { overdueDuration = time + timeInPosition }
          }

          state.events.turn.push({
            startTime: time,
            turnType,
            timerReset,
            bellowThreshold,
            overdueDuration,
            lastIntervalMinutes,
            turnDetected: eventStatus
          })
        }
      }

      // check if can merge identical repositions
      for (let i = 1; i < state.events.repositionInterval.length; i++) {
        const corrent = state.events.repositionInterval[i]
        const prev = state.events.repositionInterval[i - 1]

        if (corrent.startTime === prev.endTime && corrent.intervalMinutes === prev.intervalMinutes) {
          // merge events
          state.events.repositionInterval[i - 1].endTime = state.events.repositionInterval[i].endTime
          state.events.repositionInterval.splice(i, 1)
          i--
        }
      }
    }
  },
  actions: {

  }
})
